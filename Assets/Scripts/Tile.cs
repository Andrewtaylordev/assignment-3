﻿using UnityEngine;
using System.Collections;
using UnityEngine.Analytics;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    // Grabs transform of child object, as well as the collider
    private void Start()
    {
        m_child = this.transform.GetChild(0);
        m_collider = GetComponent<Collider>();
    }

    // Initiates Tile Selected functions from GameManager script
    private void OnMouseDown()
    {
        Debug.Log("Click");
        AnalyticsEvent.Custom("Player Click");

        m_manager.TileSelected(this);
    }

    //
    public void Initialize(GameManager manager, GameManager.Symbols symbol)
    {
        m_manager = manager;
        Symbol = symbol;
    }

    // Reveals a tile that is clicked
    public void Reveal()
    {
        Debug.Log("Reveal");
        AnalyticsEvent.Custom("Reveal Tile");

        StopAllCoroutines();
        StartCoroutine(Spin(180, 0.8f));
        m_collider.enabled = false;
    }

    // Hides Tiles
    public void Hide()
    {
        Debug.Log("Hide");
        AnalyticsEvent.Custom("Mismatch");

        StopAllCoroutines();
        StartCoroutine(Spin(0, 0.8f));
        m_collider.enabled = true;
    }

    // Every time a tile is spun, or clicked
    private IEnumerator Spin(float target, float time)
    {
        
       

        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while (timer < time)
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle(startingRotation, target, timer / time);
            m_child.eulerAngles = euler;
            yield return null;

           
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
